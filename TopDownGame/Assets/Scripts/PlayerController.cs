﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float speed,jumpForce,dirX, moveHorizontal, moveVertical;             //Floating point variable to store the player's movement speed.
    Vector2 movement;
    private Rigidbody2D rb2d;
    public bool isWalking, isIdle;
    Animator anim;
        
        //Store a reference to the Rigidbody2D component required to use 2D Physics.

    // Use this for initialization
    void Start()
    {
        //Get and store a reference to the Rigidbody2D component so that we can access it.
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    //FixedUpdate is called at a fixed interval and is independent of frame rate. Put physics code here.
    void Update()
    {
        //Store the current horizontal input in the float moveHorizontal.
        moveHorizontal = Input.GetAxis("Horizontal");

        //Store the current vertical input in the float moveVertical.
        moveVertical = Input.GetAxis("Vertical");

        //Use the two store floats to create a new Vector2 variable movement.
        movement = new Vector2(moveHorizontal,0);


            var move = new Vector3(moveHorizontal, moveVertical, 0);
            transform.position += move * speed * Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Space)){

            movement = new Vector2(rb2d.velocity.x, jumpForce);
            rb2d.velocity = movement;
        }

        setAnimationState();
    }


    void setAnimationState()
    {

        if (moveHorizontal == 0)
        {
            print("NOT WALKING");
            isWalking = false; ;
            isIdle = true; ;
            anim.SetBool("isIdle", true);
            anim.SetBool("isWalking", false);

        }
        else if (moveHorizontal != 0) {
            print("WALKING!!!");
            isWalking = true;
            isIdle = false;
            anim.SetBool("isWalking",true);
            anim.SetBool("isIdle", false);

        }
    }
}